﻿DROP TABLE IF EXISTS data_cardiff;
DROP SEQUENCE IF EXISTS cat_id_seq;

CREATE SEQUENCE cat_id_seq;

-- Create tables
CREATE TABLE data_cardiff
(
	id bigint NOT NULL DEFAULT nextval('cat_id_seq'),
	Patient character varying NOT NULL,
	DatePET	date,
	Age numeric,
	Gender numeric,
	DateDiagnosed date,
	Censor numeric,
	Surv_Post_Diag	numeric,
	DateSurvUpdate	date,
	Histology	numeric,
	RadiolTNM7stage	numeric,
	CurativeTx	numeric,
	Treatment	numeric,
	ATv1SUVmax	numeric,
	ATv1SUVmean	numeric,
	ATv1MTV	numeric,
	ATv1TLG	numeric,
	ATv1StDev	numeric,
	ATv1HistEntropy	numeric,
	ATv1HistEnergy	numeric,
	ATv1HistSkewness	numeric,
	ATv1HistKurtosis	numeric,
	ATv1Coarseness	numeric,
	ATv1Homogeneity	numeric,
	ATv1Entropy	numeric,
	ATv1Dissimilarity	numeric,
	ATv1IntVariab	numeric,
	ATv1LAE	numeric,
	ATv1ZP	numeric,
	logTLG	numeric,
	logEnergy	numeric,
	logCoarseness	numeric,
	logHomogeneity	numeric,
	PRIMARY KEY (patient)
);

